# Driver Container master branch

## This is a Work in Progress
Look at the respective platform branches for released work.

# Driver Container
Ubuntu 18.04 [![build status](https://gitlab.com/nvidia/driver/badges/master/build.svg)](https://gitlab.com/nvidia/driver/commits/master)
Ubuntu 16.04 [![build status](https://gitlab.com/nvidia/driver/badges/master/build.svg)](https://gitlab.com/nvidia/driver/commits/master)

See https://github.com/NVIDIA/nvidia-docker/wiki/Driver-containers-(Beta)

## Versioning

Versions are labeled first with the GPU driver version, then with an internal
version for the driver-container. ex: `418.87.01-1.0.0-custom-rhcos4.1`
